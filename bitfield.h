#ifndef BITFIELD_H
#define BITFIELD_H
#include <iostream>
using namespace std;
class Bitfield
{
public:
    unsigned int *bitarray;

public:
    Bitfield (unsigned int _size) // количество бит
    {
        bitarray = new unsigned int[(_size / sizeof(unsigned int)) + 1];
        bitarray[0] = _size;
        for(unsigned int i = 1; i <= ((_size / sizeof(unsigned int)) + 1); i++)
        {
            bitarray[i] = 0;
        }
    }
    ~Bitfield()
    {
        delete [] bitarray;
    }
    int UpFlag(unsigned int bit)
    {
        if (GetFlag(bit)) return 0;
        if (bit >= bitarray[0]) return 0;
        unsigned long long int a = 1;
        unsigned long long int index = bit / (sizeof(unsigned int) * 8) + 1;
        unsigned long long int deepindex = bit % (sizeof(unsigned int) * 8);
        a <<= (deepindex - 1);
        bitarray[index] |= a;
        return 1;
    }
    int DownFlag(unsigned int bit)
    {
        if (!GetFlag(bit)) return 0;
        if (bit >= bitarray[0]) return 0;
        unsigned long long int a = 1; //4294967295
        unsigned long long int index = bit / (sizeof(unsigned int) * 8) + 1;
        unsigned long long int deepindex = bit % (sizeof(unsigned int) * 8);
        a <<= (deepindex - 1);
        bitarray[index] ^= a;
        return 1;
    }
    bool GetFlag(unsigned int bit)
    {
        if (bit >= bitarray[0]) return 0;
        unsigned long long int a = 1;
        unsigned long long int index = bit / (sizeof(unsigned int) * 8) + 1;
        unsigned long long int deepindex = bit % (sizeof(unsigned int) * 8);
        a <<= (deepindex - 1);
        unsigned long long int temp = bitarray[index];
        unsigned long long int temp1 = temp | a;
        if (temp1 != temp)
        {
            return false;
        }
        return true;
    }


};




#endif // BITFIELD_H
