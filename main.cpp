#include <iostream>
#include <bitfield.h>
using namespace std;

int main()
{

    Bitfield bitfield(32);
    cout << bitfield.bitarray[0] << " \n"<< endl;
    bitfield.UpFlag(1);
    cout << bitfield.bitarray[1] << " \n"<< endl;
    bitfield.UpFlag(2);
    cout << bitfield.bitarray[1] << " \n"<< endl;
    bitfield.GetFlag(24);
    cout << bitfield.GetFlag(2) << " \n"<< endl;
    bitfield.GetFlag(25);
    cout << bitfield.GetFlag(25) << " \n"<< endl;
    bitfield.UpFlag(29);
    cout << bitfield.bitarray[1] << " \n"<< endl;
    bitfield.DownFlag(29);
    cout << bitfield.bitarray[1] << " \n"<< endl;
    bitfield.DownFlag(24);
    cout << bitfield.bitarray[1] << " \n"<< endl;
    bitfield.UpFlag(30);
    cout << bitfield.bitarray[1] << " \n"<< endl;

    cout << " \n"<< endl;
    bitfield.UpFlag(31);
    cout << bitfield.bitarray[1] << " \n"<< endl;
    bitfield.UpFlag(32);
    cout << bitfield.bitarray[1] << " \n"<< endl;
    return 0;
}
